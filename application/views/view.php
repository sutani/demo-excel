<h1>Data Siswa</h1>
<a href="<?= ("index.php/siswa/import"); ?>"> import Data Siswa</a> <br><br>
<a href="<?= ("index.php/siswa/export"); ?>"> Export Data Siswa</a> <br><br>

<table border="1" cellpadding="8">
    <tr>
        <th>NIS</th>
        <th>Nama</th>
        <th>Jenis Kelamin</th>
        <th>Alamat</th>
    </tr>

    <?php 
        if(! empty($siswa)) {
            foreach ($siswa as $data) {
                echo "<tr>";
                echo "<td>".$data->nis."</td>";
                echo "<td>".$data->nama."</td>";
                echo "<td>".$data->jenis_kelamin."</td>";
                echo "<td>".$data->alamat."</td>";
                echo "<tr>";
            }
        } else {
            echo "<tr><td colspan='4'>Data Siswa Tidak Ada</td></tr>";
        }
    
    ?>